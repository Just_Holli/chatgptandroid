package net.intersvyaz.chatgpt

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.text.Html
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import net.intersvyaz.chatgpt.databinding.ActivityMainBinding

class ChatActivity : AppCompatActivity() {
    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    private val viewModel by viewModels<ChatViewModel>()

    private val longClickList = View.OnLongClickListener {
        val text = (it as TextView).text
        val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("Copy", text)
        clipboard.setPrimaryClip(clip)
        true
    }

    private val chatAdapter = ChatAdapter(longClickList)
    private val inputMethodManager by lazy {
        getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    }

    private fun hideKeyBoard() =
        inputMethodManager.hideSoftInputFromWindow(currentFocus?.windowToken, 0)

    private fun setUpRecycler() {
        binding.tvResponse.apply {
            adapter = chatAdapter
            layoutManager = LinearLayoutManager(this@ChatActivity).apply {
                orientation = LinearLayoutManager.VERTICAL
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setUpRecycler()
        binding.btnSend.setOnClickListener {
            val input = binding.etInput.text.toString().trim()
            if (input.isNotEmpty()) {
                val item = MyItem.UserItem(
                    text = input
                )
                viewModel.dialogMessageList.add(item)
                chatAdapter.updateList(viewModel.dialogMessageList)
                binding.tvResponse.smoothScrollToPosition(
                    (binding.tvResponse.adapter?.itemCount ?: 0) - 1
                )
                hideKeyBoard()
                viewModel.sendMessage()
                binding.etInput.setText("")
            }
        }

        viewModel.response.observe(this) { state ->
            when (state) {
                is UIState.Loading -> {
                    binding.progressBar.visibility = View.VISIBLE
                    binding.btnSend.isEnabled = false
                }
                is UIState.Success -> {
                    binding.btnSend.isEnabled = true
                    binding.progressBar.visibility = View.GONE
                    val text = Html.fromHtml(state.data).toString()
                    val item = MyItem.GptItem(
                        text = text
                    )
                    viewModel.dialogMessageList.add(item)
                    chatAdapter.updateList(viewModel.dialogMessageList)
                }
                is UIState.Error -> {
                    binding.btnSend.isEnabled = true
                    binding.progressBar.visibility = View.GONE
                    Toast(this).apply {
                        setText(state.exception.message.toString())
                        show()
                    }
                }
            }
        }
    }
}