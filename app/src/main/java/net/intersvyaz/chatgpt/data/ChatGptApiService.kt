package net.intersvyaz.chatgpt.data

import net.intersvyaz.chatgpt.data.model.GenerateTextResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface ChatGPTApiService {
    @POST("v1/chat/completions")
    suspend fun generateText(
        @Body prompt: GenerateTextRequest
    ): Response<GenerateTextResponse>
}
