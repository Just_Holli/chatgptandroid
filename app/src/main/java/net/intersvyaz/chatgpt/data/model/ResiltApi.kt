package net.intersvyaz.chatgpt.data.model

sealed class ResultApi<out T : Any> {
    data class Success<out T : Any>(val data: T) : ResultApi<T>()
    data class Error(val exception: Exception) : ResultApi<Nothing>()
    object Loading : ResultApi<Nothing>()

    companion object {
        fun <T : Any> success(data: T): ResultApi<T> = Success(data)
        fun error(exception: Exception): ResultApi<Nothing> = Error(exception)
        fun loading(): ResultApi<Nothing> = Loading
    }
}