package net.intersvyaz.chatgpt.data.model

import com.google.gson.annotations.SerializedName

data class GenerateTextResponse(
    val id: String,
    @SerializedName("object")
    val obj: String,
    val created: Long,
    val model: String,
    val choices: List<Choice>,
    val usage: Usage
)

data class Choice(
    val message: Message,
    val finish_reason: String,
    val index: Int
)

data class Usage(
    val prompt_tokens: Int,
    val completion_tokens: Int,
    val total_tokens: Int
)

data class Message(
    val role: String,
    val content: String
)




