package net.intersvyaz.chatgpt.data

import com.google.gson.annotations.SerializedName
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import net.intersvyaz.chatgpt.data.model.GenerateTextResponse
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Response

class ChatGPTRepository {
    private val chatGPTApi = ApiFactory.getClient(ChatGPTApiService::class.java)

    fun getChatResponse(input: List<Message>): Flow<Result<GenerateTextResponse>> = flow {
        val request = GenerateTextRequest(
            modelName = "gpt-3.5-turbo-0301",
            message = input,
            temperature = 0.7f,
            maxTokens = 1200,
        )

        val response = chatGPTApi.generateText(
            prompt = request
        ).toResult()

        emit(response)
    }.flowOn(Dispatchers.IO)
}

fun <T : Any> Response<T>.toResult(): Result<T> {
    if (isSuccessful) {
        val body = body()
        if (body != null) {
            return Result.success(body)
        }
    }
    val errorBody = errorBody()?.string()
    val errorMessage = if (errorBody.isNullOrEmpty()) {
        message()
    } else {
        try {
            val json = JSONObject(errorBody)
            json.getString("message")
        } catch (e: JSONException) {
            errorBody
        }
    }
    return Result.failure(Exception(errorMessage))
}

data class GenerateTextRequest(
    @SerializedName("model")
    val modelName: String,
    @SerializedName("messages")
    val message: List<Message>,
    @SerializedName("temperature")
    val temperature: Float = 0.7f,
    @SerializedName("max_tokens")
    val maxTokens: Int = 60,
)

data class Message(
    val role: String,
    val content: String
)
