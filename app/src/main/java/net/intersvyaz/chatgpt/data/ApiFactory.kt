package net.intersvyaz.chatgpt.data

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ApiFactory {
    private val httpLoggingInterceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY // Устанавливаем желаемый уровень логирования
    }


    private val okHttpClient: OkHttpClient by lazy {
        OkHttpClient.Builder().addInterceptor { chain ->
            val request = chain.request().newBuilder()
                .addHeader("Authorization", "Bearer $API_KEY")
                .build()
            chain.proceed(request)
        }
            .connectTimeout(300000, TimeUnit.SECONDS)
            .callTimeout(300000, TimeUnit.SECONDS)
            .readTimeout(300000, TimeUnit.SECONDS)
            .writeTimeout(300000, TimeUnit.SECONDS)
            .addInterceptor(httpLoggingInterceptor)
            .build()
    }

    fun <T> getClient(apiService: Class<T>): T {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        return retrofit.create(apiService)
    }

    private const val BASE_URL = "https://api.openai.com"
    private const val API_KEY = "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"
}
