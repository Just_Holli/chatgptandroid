package net.intersvyaz.chatgpt

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import net.intersvyaz.chatgpt.data.ChatGPTRepository
import net.intersvyaz.chatgpt.data.Message
import okhttp3.internal.toImmutableList

class ChatViewModel : ViewModel() {

    private val chatGPTRepository = ChatGPTRepository()

    private val _response = MutableLiveData<UIState<String>>()
    val response: LiveData<UIState<String>>
        get() = _response

    val dialogMessageList = mutableListOf<MyItem>()


    fun sendMessage() {
        viewModelScope.launch {
            _response.postValue(UIState.Loading)
            val requestList = mutableListOf<Message>()

            dialogMessageList.map {
                when (it) {
                    is MyItem.UserItem -> requestList.add(
                        Message(
                            role = "user",
                            content = it.text
                        )
                    )
                    is MyItem.GptItem -> requestList.add(
                        Message(
                            role = "assistant",
                            content = it.text
                        )
                    )
                }
            }.takeLast(4)

            chatGPTRepository.getChatResponse(requestList.toImmutableList()).collect {
                it.onSuccess { response ->
                    _response.postValue(UIState.Success(response.choices.first().message.content))
                }
                it.onFailure {
                    _response.postValue(UIState.Error(it))
                }
            }
        }
    }
}

sealed class UIState<out T> {
    object Loading : UIState<Nothing>()
    data class Success<T>(val data: T) : UIState<T>()
    data class Error(val exception: Throwable) : UIState<Nothing>()
}