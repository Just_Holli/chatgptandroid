package net.intersvyaz.chatgpt

import android.util.Log
import android.view.LayoutInflater
import android.view.View.OnLongClickListener
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import net.intersvyaz.chatgpt.databinding.GptItemBinding
import net.intersvyaz.chatgpt.databinding.UserItemBinding

class ChatAdapter(private val longPressClickListener: OnLongClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>(
    ) {
    private var items: List<MyItem> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        Log.e("Items", items.toString())
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            USER_VIEW_TYPE -> {
                val view = UserItemBinding.inflate(inflater, parent, false)
                UserViewHolder(view)
            }
            GPT_VIEW_TYPE -> {
                val view = GptItemBinding.inflate(inflater, parent, false)
                GptViewHolder(view)
            }
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }

    fun updateList(newItems: List<MyItem>) {
        Log.e("Items", newItems.toString())
        items = newItems
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        Log.e("Items", items.toString())
        val myItem = items[position]

        when (getItemViewType(position)) {
            USER_VIEW_TYPE -> (holder as UserViewHolder).bind(myItem as MyItem.UserItem)
            GPT_VIEW_TYPE -> (holder as GptViewHolder).bind(myItem as MyItem.GptItem)
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }

    override fun getItemCount(): Int {
        Log.e("Items", "Position")
        return items.size
    }

    inner class UserViewHolder(private val binding: UserItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: MyItem.UserItem) {
            binding.text.setOnLongClickListener(longPressClickListener)
            binding.text.text = item.text
        }
    }

    inner class GptViewHolder(private val binding: GptItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: MyItem.GptItem) {
            binding.text.setOnLongClickListener(longPressClickListener)
            binding.text.text = item.text
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (items[position]) {
            is MyItem.UserItem -> USER_VIEW_TYPE
            is MyItem.GptItem -> GPT_VIEW_TYPE
        }
    }

    companion object {
        private const val USER_VIEW_TYPE = 0
        private const val GPT_VIEW_TYPE = 1
    }
}

sealed class MyItem {
    data class UserItem(val text: String) : MyItem()
    data class GptItem(val text: String) : MyItem()
}